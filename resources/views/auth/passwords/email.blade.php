@extends('layouts.app')

@section('content')
    <div class="bg-white w-2/3 mx-auto shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
        @include('partials.messages')

        <form action="{{ route('password.email') }}" method="post">
            @csrf

            <div class="mb-4">
                <label for="email" class="block text-grey-darker text-sm font-bold mb-2">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback text-red-500" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <button type="submit" class="bg-blue-300 hover:bg-blue-500 font-bold py-2 px-4 rounded">
                {{ __('Send Password Reset Link') }}
            </button>
        </form>
    </div>
@endsection
