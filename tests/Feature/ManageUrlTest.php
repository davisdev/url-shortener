<?php

namespace Tests\Feature;

use App\Models\Url;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ManageUrlTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var string
     */
    private $testUrl = 'https://google.com';

    /**
     * @test
     */
    public function visiting_dashboard_provides_user_with_an_option_to_create_short_url()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
             ->get('/')
             ->assertSee('Create short URL');
    }

    /**
     * @test
     */
    public function submitting_a_valid_url_redirects_back_to_personal_url_page_consisting_created_url()
    {
        $user       = factory(User::class)->create();
        $attributes = [
            'url' => $this->testUrl,
            'ttl' => 5,
        ];

        $this->actingAs($user)
             ->post(route('url.store'), $attributes)
             ->assertRedirect(route('home.url'));

        $this->actingAs($user)
             ->get(route('home.url'))
             ->assertSee($attributes['url']);
    }

    /**
     * @test
     */
    public function opening_shortened_url_should_redirect_to_its_full_url()
    {
        $this->actingAs(factory(User::class)->create());

        $url = Url::generate($this->testUrl);

        $this->get($url->short_url)->assertRedirect($url->url);
    }

    /**
     * @test
     */
    public function opening_an_expired_url_deletes_given_url()
    {
        $url = Url::generate($this->testUrl, now()->subMinute());

        $this->assertDatabaseHas('urls', $url->only('short_url'));

        // Hitting an expired endpoint would delete the given URL from database.
        $this->get($url->short_url)->assertSessionHasErrors();

        $this->assertDatabaseMissing('urls', $url->only('short_url'));
    }

    /**
     * @test
     */
    public function url_submitted_with_time_to_live_expires_after_set_expiration_time()
    {
        $timeToLive = 5;

        $this->actingAs(factory(User::class)->create())
             ->post(route('url.store'), [
                 'url' => $this->testUrl,
                 'ttl' => $timeToLive,
             ]);

        $url = Url::first();

        $this->assertFalse($url->hasExpired());
        Carbon::setTestNow(now()->addMinutes($timeToLive + 1));
        $this->assertTrue($url->hasExpired());
    }

    /**
     * @test
     */
    public function trying_to_set_an_invalid_time_to_live_for_url_throws_validation_exception()
    {
        $upperExpireLimit = config('url.max_ttl_allowed');
        $this->actingAs(factory(User::class)->create());

        $this->post(route('url.store'), [
            'url' => $this->testUrl,
            'ttl' => $upperExpireLimit + 1,
        ])->assertSessionHasErrors();

        $this->post(route('url.store'), [
            'url' => $this->testUrl,
            'ttl' => -1,
        ])->assertSessionHasErrors();
    }

    /**
     * @test
     */
    public function trying_to_set_a_time_to_live_for_url_as_zero_saves_url_permanently()
    {
        $this->actingAs(factory(User::class)->create())
             ->post(route('url.store'), [
                 'url' => $this->testUrl,
                 'ttl' => 0,
             ]);

        $this->assertTrue(Url::first()->isPermanent());
    }

    /**
     * @test
     */
    public function a_user_can_delete_his_own_shortened_urls()
    {
        $this->actingAs(factory(User::class)->create());
        $url = Url::generate($this->testUrl);

        $this->assertDatabaseHas('urls', $url->only(['url', 'short_url']));
        $this->delete(route('url.destroy', $url->short_url))->assertRedirect(route('home.url'));
        $this->assertDatabaseMissing('urls', $url->only(['url', 'short_url']));
    }

    /**
     * @test
     */
    public function a_user_cannot_delete_other_user_made_urls()
    {
        [$user1, $user2] = factory(User::class, 2)->create();

        $this->actingAs($user1);
        $url = Url::generate($this->testUrl);

        $this->actingAs($user2)
             ->delete(route('url.destroy', $url->short_url))->assertStatus(403);

        $this->assertDatabaseHas('urls', $url->only('url', 'short_url'));
    }
}
