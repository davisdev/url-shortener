<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        /**
         * Implementing caching for this (as well as cache busting for when
         * new url's get generated, seems outside of the scope for this
         * small project, so going to just load the count on every request.
         */
        $urlCount = auth()->user()->getUrlCount();

        return view('dashboard.index', compact('urlCount'));
    }

    /**
     * @return Application|Factory|View
     */
    public function url()
    {
        return view('dashboard.url');
    }
}
