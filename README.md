### About the project
Allowing users to make their own shortened URL which are auto-generated at the back-end.
Making short URL's to be customizable would present huge validation issues.

### How to
1. Register and log in
2. Screen will present itself where you can enter URL you want to shorten and set predefined time for it to expire (or be permanent)
3. If you decide you don't need a URL, you can delete it (visiting expired short url will delete it)
4. Additionally, scheduler can be run with `php artisan schedule:run` to do interval cleanups for stale urls.
