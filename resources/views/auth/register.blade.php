@extends('layouts.app')

@section('content')
    <div class="auth-callout w-2/3 mx-auto">
        {{ __("Already have an account? Go and") }} <a href="{{ route('login') }}" class="underline">{{ __('log in') }}.</a>
    </div>

    <div class="bg-white w-2/3 mx-auto shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
        <form action="{{ route('register') }}" method="post">
            @csrf

            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="name">
                    {{ __('Name') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" id="name" type="text" name="name" required value="{{ old('name') }}">

                @error('name')
                    <span class="invalid-feedback text-sm text-red-500" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                    {{ __('E-mail') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" id="email" type="text" name="email" required value="{{ old('email') }}">

                @error('email')
                    <span class="invalid-feedback text-sm text-red-500" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    {{ __('Password') }}
                </label>
                <input class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3" id="password" type="password" name="password" required>

                @error('password')
                    <span class="invalid-feedback text-sm text-red-500" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password_confirmation">
                    {{ __('Confirm password') }}
                </label>
                <input class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3" id="password_confirmation" type="password" name="password_confirmation" required>

                @error('password_confirmation')
                    <span class="invalid-feedback text-sm text-red-500" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="flex items-center justify-between">
                <button class="bg-blue-300 hover:bg-blue-500 font-bold py-2 px-4 rounded" type="submit">
                    {{ __('Register') }}
                </button>
            </div>
        </form>
    </div>
@endsection
