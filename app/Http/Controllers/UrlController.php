<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUrlRequest;
use App\Models\Url;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class UrlController extends Controller
{
    /**
     * @param StoreUrlRequest $request
     *
     * @return Application|Factory|RedirectResponse|View
     */
    public function store(StoreUrlRequest $request)
    {
        try {
            Url::generate($request->get('url'), $this->getExpirationTime($request));

            return redirect(route('home.url'));
        } catch (\Exception $e) {
            return back()->withErrors($e->getMessage());
        }
    }

    /**
     * @param string $shortUrlSeed
     *
     * @throws \Exception
     * @return Application|RedirectResponse|Redirector
     */
    public function show(string $shortUrlSeed)
    {
        $url = Url::firstWhere('short_url', $shortUrlSeed);

        if (! $url) {
            return redirect(route('home.url'))
                ->withErrors(__('Shortened URL does not exist in system.'));
        }

        if ($url->hasExpired()) {
            $url->delete();

            return redirect(route('home.url'))
                ->withErrors(__('URL you are trying to visit, has expired.'));
        }

        return redirect($url->url);
    }

    /**
     * @param string $shortUrl
     *
     * @throws \Exception
     * @return mixed
     */
    public function destroy(string $shortUrl)
    {
        $url = Url::firstWhere('short_url', $shortUrl);

        abort_if($url->user_id !== auth()->id(), 403);

        $url->delete();

        return redirect(route('home.url'))
            ->withSuccess(__('URL successfully deleted!'));
    }

    /**
     * @param Request $request
     *
     * @return Carbon|null
     */
    private function getExpirationTime(Request $request): ?Carbon
    {
        if ((int) $request->get('ttl') === 0) {
            return null;
        }

        return now()->addMinutes((int) $request->get('ttl'));
    }
}
