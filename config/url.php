<?php

return [
    /**
     * NB: Every time interval is set in minutes.
     *
     * By default, setting the upper TTL (time to live) limit to 1 week.
     * TTL more than a 1 week just makes more sense to be set to permanent.
     */
    'max_ttl_allowed' => env('URL_MAX_TTL', 60*24*7),
];
