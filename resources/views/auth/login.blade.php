@extends('layouts.app')

@section('content')
    <div class="auth-callout w-2/3 mx-auto">
        {{ __("Don't have an account? Go and") }} <a href="{{ route('register') }}" class="underline">{{ __('register') }}.</a>
    </div>

    <div class="bg-white w-2/3 mx-auto shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
        @include('partials.messages')

        <form action="{{ route('login') }}" method="post">
            @csrf

            <div class="mb-4">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                    {{ __('E-mail') }}
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" id="email" type="text" name="email" required value="{{ old('email') }}">
            </div>
            <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    {{ __('Password') }}
                </label>
                <input class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3" id="password" type="password" name="password" required>
                <p class="text-red text-xs italic">{{ __('Please choose a password') }}</p>
            </div>
            <div class="flex items-center justify-between">
                <button class="bg-blue-300 hover:bg-blue-500 font-bold py-2 px-4 rounded" type="submit">
                    {{ __('Sign In') }}
                </button>
                <a class="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker" href="{{ route('password.request') }}">
                    {{ __('Forgot Password?') }}
                </a>
            </div>
        </form>
    </div>
@endsection
