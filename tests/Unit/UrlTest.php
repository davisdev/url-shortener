<?php

namespace Tests\Unit;

use App\Models\Url;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UrlTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var string
     */
    private $testUrl = 'https://test-url.com';

    /**
     * @test
     */
    public function it_creates_a_unique_short_url()
    {
        $url = Url::generate($this->testUrl);

        $this->assertDatabaseHas('urls', $url->only(['url', 'short_url']));
    }

    /**
     * @test
     */
    public function it_correctly_checks_url_uniqueness()
    {
        Url::generate($this->testUrl);

        $this->assertFalse(Url::unique($this->testUrl));
    }

    /**
     * @test
     */
    public function it_throws_an_exception_if_invalid_url_is_provided()
    {
        $this->expectExceptionMessage('Invalid and\or used URL provided.');

        $url = Url::generate('not-really-a-url');

        $this->assertDatabaseMissing('urls', $url->toArray());
    }
}
