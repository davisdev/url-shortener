@extends('layouts.app')

@section('content')
    <a href="{{ route('home.index') }}" class="block my-5 underline">{{ __('Home') }}</a>

    @include('partials.messages')

    @if (auth()->user()->urls->count())
        <table class="table-auto w-full">
            <thead class="bg-gray-200">
                <th class="w-2/5 px-4 py-2 border">{{ __('Original URL') }}</th>
                <th class="w-2/5 px-4 py-2 border">{{ __('Shortened URL') }}</th>
                <th class="w-1/5 px-4 py-2 border">{{ __('Expires at') }}</th>
            </thead>
            @foreach(auth()->user()->urls as $url)
                <tr class="@if ($loop->iteration % 2 === 0) bg-gray-100 @endif @if($url->getExpirationStatus() === __('Expired')) text-gray-500 @endif">
                    <th>
                        <a href="{{ url($url->url) }}" class="hover:underline">{{ url($url->url) }}</a>
                    </th>
                    <th>
                        <a href="{{ url($url->short_url) }}" class="hover:underline">{{ url($url->short_url) }}</a>
                        <form action="{{ route('url.destroy', $url->short_url) }}" method="post">
                            @csrf
                            @method('DELETE')

                            <button type="submit">(delete)</button>
                        </form>
                    </th>
                    <th>{{ $url->getExpirationStatus() }}</th>
                </tr>
            @endforeach
        </table>
    @else
        <div class="py-4 px-4 bg-blue-300 rounded">
            {{ __("No URL's created.") }} <a href="{{ route('home.index') }}" class="pl-2 underline">{{ __('Want to create one?') }}</a>
        </div>
    @endif
@endsection
