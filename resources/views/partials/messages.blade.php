@if (count($errors))
    <ul class="py-4 px-4 list-none bg-red-300 rounded-sm p-2 mb-2">
        @foreach($errors->all() as $error)
            <li class="py-1">{{ $error }}</li>
        @endforeach
    </ul>
@endif

@if (session()->has('success'))
    <ul class="py-4 px-4 list-none bg-green-300 rounded-sm p-3 mb-2">
        <li class="py-1">{{ session()->get('success') }}</li>
    </ul>
@endif

@if (session()->has('warning'))
    <ul class="py-4 px-4 list-none bg-orange-300 rounded-sm p-3 mb-2">
        <li class="py-1">{{ session()->get('warning') }}</li>
    </ul>
@endif

@if (session()->has('status'))
    <ul class="py-4 px-4 list-none bg-blue-300 rounded-sm p-3 mb-2">
        <li class="py-1">{{ session()->get('status') }}</li>
    </ul>
@endif
