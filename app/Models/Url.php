<?php

namespace App\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class Url extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'url',
        'short_url',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'expires_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer'
    ];

    /**
     * @var int
     */
    private $shortUrlSeedLength = 5;

    /**
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param string $from
     * @param Carbon $expirationDateTime
     *
     * @throws Exception
     * @return Url
     */
    public static function generate(string $from, Carbon $expirationDateTime = null)
    {
        $instance = new self;

        if (! $instance->validUrl($from)) {
            throw new Exception('Invalid and\or used URL provided.');
        }

        return $instance->forceCreate([
            'user_id' => auth()->id() ?? null,
            'url' => strtolower($from),
            'short_url' => self::generateSeed($instance),
            'expires_at' => self::getExpirationTime($expirationDateTime)
        ]);
    }

    /**
     * @param $query
     *
     * @return Builder
     */
    public static function scopeExpired($query): Builder
    {
        return $query->where('expires_at', '<', now()->toDateTimeString());
    }

    /**
     * @param string $seed
     *
     * @return bool
     */
    public static function unique(string $seed): bool
    {
        return ! self::where('url', $seed)->count();
    }

    /**
     * @param $url
     *
     * @return bool
     */
    private function validUrl($url): bool
    {
        return filter_var($url, FILTER_VALIDATE_URL)
               && ! $this->where('user_id', auth()->id())
                         ->where('url', $url)->count();
    }

    /**
     * @param Url $instance
     *
     * @return string
     */
    protected static function generateSeed(Url $instance): string
    {
        $seed = Str::random($instance->shortUrlSeedLength);

        while ( ! $instance->unique($seed)) {
            $seed = Str::random($instance->shortUrlSeedLength);
        }

        return $seed;
    }

    /**
     * @param Carbon|null $expirationDateTime
     *
     * @return Carbon|null
     */
    protected static function getExpirationTime(?Carbon $expirationDateTime)
    {
        return $expirationDateTime === null ? null : $expirationDateTime;
    }

    /**
     * @return bool
     */
    public function hasExpired()
    {
        return ! $this->isPermanent() && $this->expires_at->lt(now());
    }

    /**
     * @return bool
     */
    public function isPermanent()
    {
        return $this->expires_at === null;
    }

    /**
     * @return Carbon|string
     */
    public function getExpirationStatus()
    {
        if ($this->expires_at === null) return __('Never');

        return now()->diffInSeconds($this->expires_at, false) <= 0
            ? __('Expired')
            : $this->expires_at->diffForHumans();
    }
}
