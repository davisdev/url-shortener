<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'DashboardController@index')->name('home.index');
    Route::get('url', 'DashboardController@url')->name('home.url');

    Route::post('url', 'UrlController@store')->name('url.store');
    Route::delete('url/{shortUrl}', 'UrlController@destroy')->name('url.destroy');
});

// Any other route will just be considered as short url and dealt in such way.
Route::any('{shortUrlSeed}', 'UrlController@show')->name('url.show');
