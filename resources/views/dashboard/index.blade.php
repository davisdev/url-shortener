@extends('layouts.app')

@section('content')
    <p class="text-2xl my-5">
        {{ __('Create short URL') }}
    </p>

    @include('partials.messages')

    <a href="{{ route('home.url') }}" class="block my-5 underline">{{ __("My URL's shortened") }} ({{ $urlCount }})</a>

    <form action="{{ route('url.store') }}" method="post">
        @csrf

        <label for="url" class="block">{{ __('Enter URL you want to shorten') }}</label>
        <input type="text" name="url" id="url" class="url-input" placeholder="E.g https://google.com">

        <label for="ttl" class="mr-2">{{ __('Expiration time') }}</label>
        <select name="ttl" id="ttl"
                class="mr-5 cursor-pointer appearance-none border bg-white border-blue-500 py-3 px-4 pr-8 rounded leading-tight focus:outline-none">
            <option value="1">{{ __('1 min') }}</option>
            <option value="5">{{ __('5 min') }}</option>
            <option value="10">{{ __('10 min') }}</option>
            <option value="30">{{ __('30 min') }}</option>
            <option value="60">{{ __('1 h') }}</option>
            <option value="1440">{{ __('1 day') }}</option>
            <option value="0">{{ __('Forever') }}</option>
        </select>

        <button type="submit" class="py-2 px-5 rounded bg-blue-500 hover:bg-blue-400 text-white">{{ __('Shorten') }}</button>
    </form>
@endsection
